define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/controls/controls',
	'madjoh_modules/page_transition/page_transition',
	'madjoh_modules/sweep_event/sweep_event',
	'madjoh_modules/styling/styling'
],
function(require, CustomEvents, Controls, PageTransition, SweepEvent, Styling){
	// --- SWEEP TRANSITION ---
	var SweepTransition = {
		// --- REGEXP ---
		sweepCtrlString : 'activeSweeper',
		sweepCtrlReg : new RegExp('(\\s|^)'+'activeSweeper'+'(\\s|$)'),

		// --- CONFLICT VARIABLES ---
		movingGroupIndex : null,
		toSweepingPageIndex : null,
		sweepAnimationFinished : false,

		// --- CUSTOMISATION ---
		transitionSpeed : '0.5s',

		// --- SEQUENCIVE ANIMATION ---
		onPageSweepEnd : function(e){
			var source = e.target;
			var args = source.parentNode.id.split('_');

			if(args[args.length-1] == SweepTransition.movingGroupIndex){
				var animName = source.style.webkitAnimationName;
				if(!animName) animName = source.style.animationName;
				
				if(animName=='pageOutToLeft' || animName=='pageOutToRight'){
					Styling.removeClass(source, PageTransition.activeString);
					
					// EVENTS
					CustomEvents.fireCustomEvent(source, 'sweepOutEnd');
					if(animName=='pageOutToLeft') CustomEvents.fireCustomEvent(source, 'sweepOutLeftEnd');
					else if(animName=='pageOutToRight') CustomEvents.fireCustomEvent(source, 'sweepOutRightEnd');
				}else if(animName=='pageInFromLeft' || animName=='pageInFromRight'){
					// EVENTS
					CustomEvents.fireCustomEvent(source, 'sweepInEnd');
					if(animName=='pageInFromLeft') CustomEvents.fireCustomEvent(source, 'sweepInLeftEnd');
					else if(animName=='pageInFromRight') CustomEvents.fireCustomEvent(source, 'sweepInRightEnd');
				}

				if(SweepTransition.sweepAnimationFinished){
					PageTransition.endTransition();

					var activs = source.parentNode.querySelectorAll('#'+source.parentNode.id+'>.sweepingPage');
					for(var i=0; i<activs.length; i++){
						activs[i].style.webkitAnimation = '';
						activs[i].style.animation = '';
					}
					SweepTransition.sweepToPageParam(args[args.length-1]);
				}else SweepTransition.sweepAnimationFinished = true;
			}
		},
		changeSweeperImg : function(page, active){
			var args = page.parentNode.id.split('_');

			var buttons = document.querySelectorAll('#sweeper_'+args[args.length-1]+'_'+page.pageId+' img.sweeperImg');
			for(var i=0; i<buttons.length; i++){
				var path = buttons[i].src;
				var newPath;
				if(active) newPath = path.substr(0,path.length-4)+'_clique.png';
				else newPath = path.substr(0,path.length-11)+'.png';

				buttons[i].src = newPath;
			}		
		},

		// --- SWEEP TO PRECISE PAGE ---
		sweepToPage : function(e){
			var source = e.target;
			while(source.className.indexOf('pageSweeper')<0) source = source.parentNode;

			var args = source.id.split('_');
			var groupID = args[args.length-2];
			SweepTransition.toSweepingPageIndex = parseInt(args[args.length-1]);

			SweepTransition.sweepToPageParam(groupID)
		},
		sweepToPageParam : function(groupID){
			var activeSweepingPage = document.querySelector('#sweepingGroup_'+groupID+'> .'+PageTransition.activeString);
			var activeSweepingPageIndex = parseInt(activeSweepingPage.pageId);

			if(SweepTransition.toSweepingPageIndex>activeSweepingPageIndex) SweepTransition.sweepToNextPage(true, groupID);
			else if(SweepTransition.toSweepingPageIndex<activeSweepingPageIndex) SweepTransition.sweepToNextPage(false, groupID);
		},

		// --- SWEEP IN A LOOP ---
		circularSweep : function(e){
			var source = e.target;
			while(source.className.indexOf('nextSweeper')<0) source = source.parentNode;

			var args = source.id.split('_');
			SweepTransition.circularSweepParam(args[args.length-2], args[args.length-1]);
		},
		circularSweepParam : function(groupID, sens){
			var activeSweepingPage = document.querySelector('#sweepingGroup_'+groupID+'> .'+PageTransition.activeString);
			var activeSweepingPageIndex = parseInt(activeSweepingPage.pageId);

			var sweepingGroupLength = document.querySelectorAll('#sweepingGroup_'+groupID+'> .sweepingPage').length;

			if(sens=='1'){
				SweepTransition.toSweepingPageIndex = (activeSweepingPageIndex+1+sweepingGroupLength)%sweepingGroupLength;
				SweepTransition.sweepToNextPage(true, groupID);
			}else{
				SweepTransition.toSweepingPageIndex = (activeSweepingPageIndex-1+sweepingGroupLength)%sweepingGroupLength;
				SweepTransition.sweepToNextPage(false, groupID);
			}
		},
		circularTouchSweep : function(page, sens){
			var args = page.parentNode.id.split('_');
			SweepTransition.circularSweepParam(args[args.length-1], sens);
		},

		// --- FLASH SWEEPERS ---
		flashSweepers : function(e){
			var source = e.target;
			while(source.className.indexOf('flashSweeper')<0) source = source.parentNode;

			var args = source.id.split('_');
			SweepTransition.flashSweepersParam(args[args.length-2], args[args.length-1]);
		},
		flashSweepersParam : function(gpeId, pageId){
			// COMMUTE PAGES
			var activePage = document.querySelector('#sweepingGroup_'+gpeId+'>.'+PageTransition.activeString);
			var nextPage = document.querySelectorAll('#sweepingGroup_'+gpeId+'>.sweepingPage')[pageId];

			if(activePage === nextPage) return;

			Styling.removeClass(activePage, PageTransition.activeString);
			Styling.addClass(nextPage, PageTransition.activeString);

			// COMMUTE BUTTONS STATUS
			Styling.removeClass(document.getElementById('sweeper_'+gpeId+'_'+activePage.pageId), SweepTransition.sweepCtrlString);
			Styling.addClass(document.getElementById('sweeper_'+gpeId+'_'+pageId), SweepTransition.sweepCtrlString);

			// COMMUTE BULLET STATUS
			var sweepingBullets = document.querySelector('#sweepingGroup_'+gpeId+'> .sweepingBullets');
			if(sweepingBullets){
				var active = sweepingBullets.querySelector('div.active');
				if(active) Styling.removeClass(active, 'active');
				var bullets = sweepingBullets.querySelectorAll('div');
				Styling.addClass(bullets[pageId], 'active');
			}

			// EVENTS
			CustomEvents.fireCustomEvent(activePage, 'sweepOutStart');
			CustomEvents.fireCustomEvent(nextPage, 'sweepInStart');
			CustomEvents.fireCustomEvent(activePage, 'sweepOutEnd');
			CustomEvents.fireCustomEvent(nextPage, 'sweepInEnd');
		},

		// --- SWEEP TO NEXT/PREVIOUS PAGE MODULE ---
		sweepToNextPage : function(sens, groupID){
			SweepTransition.movingGroupIndex = groupID;
			PageTransition.startTransition();

			SweepTransition.sweepAnimationFinished = false;

			// GROUP LENGTH
			var sweepingGroupLength = document.querySelectorAll('#sweepingGroup_'+groupID+'> .sweepingPage').length;

			// ACTIVE PAGE OUT
			var activeSweepingPage = document.querySelector('#sweepingGroup_'+groupID+'> .'+PageTransition.activeString);
			var activeSweepingPageIndex = parseInt(activeSweepingPage.pageId);
			CustomEvents.fireCustomEvent(activeSweepingPage, 'sweepOutStart');

			if(sens){
				CustomEvents.fireCustomEvent(activeSweepingPage, 'sweepOutLeftStart');
				activeSweepingPage.style.webkitAnimation = 'pageOutToLeft '+SweepTransition.transitionSpeed;
				activeSweepingPage.style.animation = 'pageOutToLeft '+SweepTransition.transitionSpeed;
			}
			else{
				CustomEvents.fireCustomEvent(activeSweepingPage, 'sweepOutRightStart');
				activeSweepingPage.style.webkitAnimation = 'pageOutToRight '+SweepTransition.transitionSpeed;
				activeSweepingPage.style.animation = 'pageOutToRight '+SweepTransition.transitionSpeed;
			}
			activeSweepingPage.style.webkitAnimationFillMode = 'forwards';
			activeSweepingPage.style.animationFillMode = 'forwards';

			// NEXT PAGE
			var nextPageIndex;
			if(sens) nextPageIndex = (activeSweepingPageIndex+1+sweepingGroupLength)%sweepingGroupLength;
			else nextPageIndex = (activeSweepingPageIndex-1+sweepingGroupLength)%sweepingGroupLength;
			var nextPage = document.querySelectorAll('#sweepingGroup_'+groupID+'> .sweepingPage');
			nextPage = nextPage[nextPageIndex];

			// NEXT PAGE IN
			CustomEvents.fireCustomEvent(nextPage, 'sweepInStart');
			Styling.addClass(nextPage, PageTransition.activeString);
			if(sens){
				CustomEvents.fireCustomEvent(nextPage, 'sweepInRightStart');
				nextPage.style.webkitAnimation = 'pageInFromRight '+SweepTransition.transitionSpeed;
				nextPage.style.animation = 'pageInFromRight '+SweepTransition.transitionSpeed;
			}else{
				CustomEvents.fireCustomEvent(nextPage, 'sweepInLeftStart');
				nextPage.style.webkitAnimation = 'pageInFromLeft '+SweepTransition.transitionSpeed;
				nextPage.style.animation = 'pageInFromLeft '+SweepTransition.transitionSpeed;
			}
			nextPage.style.webkitAnimationFillMode = 'forwards';
			nextPage.style.animationFillMode = 'forwards';

			// COMMUTE CONTROLS STATE
			var activeSweeper = document.getElementById('sweeper_'+groupID+'_'+activeSweepingPageIndex);
			Styling.removeClass(activeSweeper, SweepTransition.sweepCtrlString);

			if(sens) activeSweepingPageIndex = (activeSweepingPageIndex+1+sweepingGroupLength)%sweepingGroupLength;
			else activeSweepingPageIndex = (activeSweepingPageIndex-1+sweepingGroupLength)%sweepingGroupLength;

			activeSweeper = document.getElementById('sweeper_'+groupID+'_'+activeSweepingPageIndex);
			Styling.addClass(activeSweeper, SweepTransition.sweepCtrlString);

			// BULLETS
			var sweepingBullets = document.querySelector('#sweepingGroup_'+groupID+'> .sweepingBullets');
			if(sweepingBullets){
				var active = sweepingBullets.querySelector('div.active');
				if(active) Styling.removeClass(active, 'active');
				var bullets = sweepingBullets.querySelectorAll('div');
				Styling.addClass(bullets[nextPageIndex], 'active');
			}
		},

		// --- REGISTER ---
		register : function(group){
			if(!group.id){
				console.log('Missing attribut id on sweeping group !!');
				return false;
			}

			var sweepingBullets = group.querySelector('#'+group.id+'> .sweepingBullets');

			var sweepingPages = group.querySelectorAll('#'+group.id+'> .sweepingPage');
			for(var j=0; j<sweepingPages.length; j++){
				sweepingPages[j].pageId = j;
				if(group.className.indexOf('internSweepingGroup')==-1){ // INTERN SWEEPING PAGES ALREADY INHERIT THE LISTENER
					sweepingPages[j].addEventListener('webkitAnimationEnd', SweepTransition.onPageSweepEnd, false);
					sweepingPages[j].addEventListener('animationend', SweepTransition.onPageSweepEnd, false);
				}

				CustomEvents.addCustomEventListener(sweepingPages[j], 'sweepOutStart', function(page){SweepTransition.changeSweeperImg(page, false);});
				CustomEvents.addCustomEventListener(sweepingPages[j], 'sweepInStart', function(page){SweepTransition.changeSweeperImg(page, true);});

				// BULLETS
				if(sweepingBullets){
					var bullet = document.createElement('div');
						bullet.className = 'flashSweeper';
						bullet.id = 'bullet_' + group.id + '_' + j;
						bullet.addEventListener(Controls.click, SweepTransition.flashSweepers, false);
					sweepingBullets.appendChild(bullet);
				}
			}

			// STARTING PAGE
			var active = group.querySelector('#'+group.id+'>.sweepingPage.'+PageTransition.activeString);
			if(!active){
				active = group.querySelector('#'+group.id+'>.sweepingPage');
				Styling.addClass(active, PageTransition.activeString);
			}

			// ACTIVE BULLET
			if(sweepingBullets){
				bullets = sweepingBullets.querySelectorAll('div');
				Styling.addClass(bullets[active.pageId], 'active');
			}
		},
		registerSweepable : function(group){
			if(!group.id){
				console.log('Missing attribut id on sweepable group !!');
				return false;
			}
			var sweepablePages = group.querySelectorAll('#'+group.id+'> .sweepingPage');
			for(var j=0; j<sweepablePages.length; j++){
				SweepEvent.addTouchListener(sweepablePages[j]);

				if(j>0) CustomEvents.addCustomEventListener(sweepablePages[j], 'sweepTouchRight', function(page){SweepTransition.circularTouchSweep(page, 0);});
				if(j<sweepablePages.length-1) CustomEvents.addCustomEventListener(sweepablePages[j], 'sweepTouchLeft', function(page){SweepTransition.circularTouchSweep(page, 1);});
			}
		}
	};

	// --- CONTROLLERS ---
		var pageSweepers = document.querySelectorAll('.pageSweeper');
		for(var i=0; i<pageSweepers.length; i++) pageSweepers[i].addEventListener(Controls.click, SweepTransition.sweepToPage, false);
		
		var nextSweepers = document.querySelectorAll('.nextSweeper');
		for(var i=0; i<nextSweepers.length; i++) nextSweepers[i].addEventListener(Controls.click, SweepTransition.circularSweep, false);
		
		var flashSweepers = document.querySelectorAll('.flashSweeper');
		for(var i=0; i<flashSweepers.length; i++) flashSweepers[i].addEventListener(Controls.click, SweepTransition.flashSweepers, false);

		var sweepableGroups = document.querySelectorAll('.sweepableGroup');
		for(var i=0; i<sweepableGroups.length; i++) SweepTransition.registerSweepable(sweepableGroups[i]);
			
	// --- SEQUENCIVE ANIMATION ---
		var sweepingGroups = document.querySelectorAll('.sweepingGroup');
		for(var i=0; i<sweepingGroups.length; i++) SweepTransition.register(sweepingGroups[i]);

	// --- ACTIVE STATE ---
		var activeSweepingPages = document.querySelectorAll('.'+PageTransition.activeString);
		for(var i=0; i<activeSweepingPages.length; i++){
			var args = activeSweepingPages[i].parentNode.id.split('_');

			var activeSweeper = document.getElementById('sweeper_'+args[args.length-1]+'_'+activeSweepingPages[i].pageId);
			if(activeSweeper) Styling.addClass(activeSweeper, SweepTransition.sweepCtrlString);
		}

	return SweepTransition;
});