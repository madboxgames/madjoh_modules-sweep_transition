# sweep_transition #

v1.1.5

This module enables pages transition with a sweep animation (a page that comes in pushes the actual page out).

## Gettings Started ##

### Defining the HTML properly ###

** For this code to work ** you need to :

- Define the ** Sweeping Groups **:
	- class = 'sweepingGroup'
	- id = 'sweepingGroup_X' where :
		- X is the group id.

- Define the ** Sweeping Pages **:
	- class = 'sweepingPage [activeSweepingPage]'
	** /!\ There can only be one activeSweepingPage in a sweepingGroup /!\ **

- Define the ** Controllers **:
	- Sweep to a ** specific page ** :
		- class = 'pageSweeper'
		- id = 'sweeper_X_Y where :
			- X is the *group id*.
			- Y is the *target page number* ** /!\ starting at 0 /!\ **

	- Sweep to ** next / previous ** :
		- class = "nextSweeper"
		- id = "blabla_X_Z" where :
			- X is the *group id*.
			- Z is the *sens of rotation* (0 : backward // 1 : forward)

	- ** Flash ** to a specific page :
		- class = "flashSweeper"
		- id = "sweeper_X_Z" where :
			- X is the *group id*.
			- Y is the *target page number* ** /!\ starting at 0 /!\ **

	- ** Sweep at contact ** :
		- class = 'sweepableGroup'

- ** Further Features ** :
	- When the Y-th sweeping page of X group is active :
		- The className of sweeper_X_Y contains 'activeSweeper'. On initialisation, the active sweeper gets its class automatically.
		- Images inside sweeper_X_Y with the class 'sweeperImg' get '_clique.png' appearance.
	- You can change the transition speed by overriding SweepTransition.transitionSpeed *(String)*. Default value is '0.5s'.

## Example ##
```html
<!DOCTYPE html>
<html>
	<head>[...]</head>
	<body>
		<div class="page-container">
			<div id="p_body">
				<div class="Page">
					<!-- SWEEPING PAGES -->
					<div class="fullTopPage sweepingGroup sweepableGroup" id="sweepingGroup_1">
						<div class="sweepingPage activeSweepingPage">
							<p>First Page</p>
						</div>
						<div class="sweepingPage">
							<p>Second Page</p>
						</div>
						<div class="sweepingPage">
							<p>Third Page</p>
						</div>
					</div>

					<!-- CONTROLLERS -->
					<footer>
						<ul>
							<li class="pageSweeper" id="sweeper_1_0">Page 1</li> <!-- No need to specify the active sweeper -->
							<li class="flashSweeper" id="sweeper_1_1">Page 2</li>
							<li class="pageSweeper" id="sweeper_1_2">Page 3</li>
						</ul>

						<ul>
							<li class="nextSweeper" id="next_sweeper_1_0">Previous</li>
							<li class="nextSweeper" id="next_sweeper_1_0">Next</li>
						</ul>
					</footer>
				</div>

				<!-- CACHE PAGE -->
                <div id="cacheBlock"></div>
			</div>
		</div>

		<script type="text/javascript" src="../[...]/SweepTransition.js"></script>
	</body>
</html>
```

### Controlling the transition with  JavaScript ###
